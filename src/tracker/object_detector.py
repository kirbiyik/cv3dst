import torch
import torch.nn.functional as F
from torch import nn
from torch.jit.annotations import Tuple, List, Dict, Optional
from torch import Tensor

from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from collections import OrderedDict
from torchvision.models.detection.transform import resize_boxes
from torchvision.models.detection._utils import BoxCoder
from torchvision.ops import boxes as box_ops



def extend_boxes(bboxes, image_sizes, extend_ratio=0.04):
    """
    bboxes: torch.Tensor [N, 4]  N: proposal num
    image_sizes: torch.Tensor [[h, w]]
    extend_ratio: float, how much each edge will be extended compared to size
    """
    w = bboxes[:,2] - bboxes[:,0]
    h = bboxes[:,3] - bboxes[:,1]
    bboxes[:,0] = bboxes[:, 0] - (extend_ratio) * w
    bboxes[:,1] = bboxes[:, 1] - (extend_ratio) * h

    bboxes[:,2] = bboxes[:, 2] + (extend_ratio) * w
    bboxes[:,3] = bboxes[:, 3] + (extend_ratio) * h
    sizes = image_sizes[0][0], image_sizes[0][1]
    bboxes = box_ops.clip_boxes_to_image(bboxes, sizes)
    return bboxes

    
    

def postprocess_detections(class_logits, box_regression, proposals, image_shapes):
        # type: (Tensor, Tensor, List[Tensor], List[Tuple[int, int]])
        score_thresh = 0.0
        nms_thresh = 1.0
        detections_per_img = 100

        device = class_logits.device
        num_classes = class_logits.shape[-1]

        boxes_per_image = [len(boxes_in_image) for boxes_in_image in proposals]
        pred_boxes = BoxCoder((10., 10., 5., 5.)).decode(box_regression, proposals)

        pred_scores = F.softmax(class_logits, -1)

        # split boxes and scores per image
        if len(boxes_per_image) == 1:
            # TODO : remove this when ONNX support dynamic split sizes
            # and just assign to pred_boxes instead of pred_boxes_list
            pred_boxes_list = [pred_boxes]
            pred_scores_list = [pred_scores]
        else:
            pred_boxes_list = pred_boxes.split(boxes_per_image, 0)
            pred_scores_list = pred_scores.split(boxes_per_image, 0)

        all_boxes = []
        all_scores = []
        all_labels = []
        for boxes, scores, image_shape in zip(pred_boxes_list, pred_scores_list, image_shapes):
            boxes = box_ops.clip_boxes_to_image(boxes, image_shape)

            # create labels for each prediction
            labels = torch.arange(num_classes, device=device)
            labels = labels.view(1, -1).expand_as(scores)

            # remove predictions with the background label
            boxes = boxes[:, 1:]
            scores = scores[:, 1:]
            labels = labels[:, 1:]

            # batch everything, by making every class prediction be a separate instance
            boxes = boxes.reshape(-1, 4)
            scores = scores.reshape(-1)
            labels = labels.reshape(-1)

            # remove low scoring boxes
            inds = torch.nonzero(scores > score_thresh).squeeze(1)
            boxes, scores, labels = boxes[inds], scores[inds], labels[inds]

            # remove empty boxes
            keep = box_ops.remove_small_boxes(boxes, min_size=1e-2)
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]

            # non-maximum suppression, independently done per class
            keep = box_ops.batched_nms(boxes, scores, labels, nms_thresh)
            # keep only topk scoring predictions
            keep, _ = torch.sort(keep[:detections_per_img]) # sort indices
            # print(keep)
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]

            all_boxes.append(boxes)
            all_scores.append(scores)
            all_labels.append(labels)

        return all_boxes, all_scores, all_labels


class FRCNN_FPN(FasterRCNN):

    def __init__(self, num_classes, nms_thresh=0.5):
        backbone = resnet_fpn_backbone('resnet50', False) # TODO: change it
        super(FRCNN_FPN, self).__init__(backbone, num_classes)
        self.prev_proposals = None
        self.prev_proposals_losses = None
        
        self.roi_heads.nms_thresh = nms_thresh
        # self.roi_heads.score_thresh = 0.25
        # self.roi_heads.detections_per_img = 100


        # remove sorting on scores while in nms since it messes up tracking algo
        self.roi_heads.postprocess_detections = postprocess_detections

        

    def detect(self, img):
        device = list(self.parameters())[0].device
        img = img.to(device)

        detections = self(img)[0]

        return detections['boxes'].detach().cpu(), detections['scores'].detach().cpu()

    def detect_with_proposals(self, images, proposal_boxes):
        with torch.no_grad():
            device = list(self.parameters())[0].device
            images = images.to(device)
            
            original_image_sizes = [[images[0].shape[-2], images[0].shape[-1]]]
            targets = None
            images, targets = self.transform(images, targets)
            features = self.backbone(images.tensors)
            if isinstance(features, torch.Tensor):
                features = OrderedDict([('0', features)])
            # assuming proposal_losses is None
            # TODO: what is image.image_sizes?
            proposal_boxes = torch.cat(proposal_boxes, dim=0).to(device)
            proposal_boxes = extend_boxes(proposal_boxes, original_image_sizes)
            proposal_boxes = resize_boxes(proposal_boxes, original_image_sizes[0], images.image_sizes[0])
            custom_roi_heads = self.roi_heads
            # custom_roi_heads.nms_thresh = 1.0
            # custom_roi_heads.score_thresh = 0.0
            detections, detector_losses = custom_roi_heads(features, [proposal_boxes],
                                                        images.image_sizes, targets)

            detections = self.transform.postprocess(detections, images.image_sizes, original_image_sizes)
            return detections[0]['boxes'].detach().cpu(), detections[0]['scores'].detach().cpu()
