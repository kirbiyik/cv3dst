import numpy as np
import torch
import torch.nn.functional as F

import motmetrics as mm
from torchvision.ops.boxes import clip_boxes_to_image, nms
from matplotlib import pyplot as plt

import PerceptualSimilarity.models
from IPython import display
import time


def get_xywh(tensor):
    """
    
    [
        [x1, y1, x2, y2] -> [x, y, h, w]
        [x1, y1, x2, y2] -> [x, y, h, w]
        [x1, y1, x2, y2] -> [x, y, h, w]
    ]
    """
    w = tensor[:,2] - tensor[:,0]
    h = tensor[:,3] - tensor[:,1]
    return torch.stack([tensor[:,0], tensor[:,1], w, h], dim=1)


class Tracker:
    """The main tracking file, here is where magic happens."""

    def __init__(self, obj_detect):
        self.obj_detect = obj_detect

        self.tracks = [] 
        self.tracks_active = [] # objects still
        self.tracks_dead_recent = [] # will store dead tracks from last n frames
        self.model = PerceptualSimilarity.models.PerceptualLoss(model='net-lin', net='alex', use_gpu=True, gpu_ids=[0])
        self.model.eval()
        self.track_num = 0
        self.im_index = 0
        self.results = {}
        self.reid_counter = 0

        self.mot_accum = None

    def reset(self, hard=True):
        self.tracks_active = []

        if hard:
            self.track_num = 0
            self.results = {}
            self.im_index = 0

    def add(self, new_boxes, new_scores):
        """Initializes new Track objects and saves them."""
        num_new = len(new_boxes)
        for i in range(num_new):
            self.tracks_active.append(Track(
                new_boxes[i],
                new_scores[i],
                self.track_num + i
            ))
        self.track_num += num_new

    def get_pos(self):
        """Get the positions of all active tracks."""
        if len(self.tracks) == 1:
            box = self.tracks[0].box
        elif len(self.tracks) > 1:
            box = torch.stack([t.box for t in self.tracks], 0)
        else:
            box = torch.zeros(0).cuda()
        return box

    def data_association(self, boxes, scores):
        self.tracks = []
        self.add(boxes, scores)

    def step(self, frame):
        """This function should be called every timestep to perform tracking with a blob
        containing the image information.
        """
        # object detection
        boxes, scores = self.obj_detect.detect(frame['img'])
        self.data_association(boxes, scores)

        # results
        for t in self.tracks:
            if t.id not in self.results.keys():
                self.results[t.id] = {}
            self.results[t.id][self.im_index] = np.concatenate([t.box.cpu().numpy(), np.array([t.score])])

        self.im_index += 1


    def tracktor_step(self, frame):
        """This function should be called every timestep to perform tracking with a blob
        containing the image information.
        """
        # object detection
        sigma_active = 0.5 # threshold to kill active box
        lambda_active = 0.6
        lambda_new = 0.4
        num_frame_recent_dead = 10 # how many frames to consider for reid

        boxes = []
        scores = []

        if len(self.tracks_active):
            active_boxes, active_scores = self.obj_detect.detect_with_proposals(frame['img'], [t.box.unsqueeze(0) for t in self.tracks_active])
            dead_track_indices = []
            dead_tracks = [] # List of list: each element stores a Track object
            for i, t in enumerate(self.tracks_active):
                b = active_boxes[i]
                s = active_scores[i]
                if s < sigma_active:
                    dead_track_indices.append(i) 
                    # last cropped img should be stored in track as well for comparison by perceptual loss network
                    t.tensor = frame['img'][0][:, int(t.box[1]):int(t.box[3]), int(t.box[0]):int(t.box[2])]
                    dead_tracks.append(t)
                    continue
                boxes.append(b)
                scores.append(s)
            
            # kill dead boxes at n'th frame before, we track at most n frames with dead boxes.
            if len(self.tracks_dead_recent) > num_frame_recent_dead:
                self.tracks_dead_recent.pop(0)
            
            # append new dead_tracks
            if len(dead_tracks):
                self.tracks_dead_recent.append(dead_tracks)
            
            # delete dead tracks from active
            mask = np.ones(len(self.tracks_active)).astype(int)
            mask[dead_track_indices] = 0
            self.tracks_active = list(np.array(self.tracks_active)[mask > 0])
            
            # add survivor tracks 
            survivors = nms(torch.stack(boxes), torch.stack(scores), lambda_active).numpy()
            self.tracks_active = list(np.array(self.tracks_active)[survivors])
            boxes = torch.stack(boxes)[survivors]
            scores = torch.stack(scores)[survivors]
            
            # update new box coordinates
            for track, box in zip(self.tracks_active, boxes):
                track.box = box

        new_boxes, new_scores = self.obj_detect.detect(frame['img'])
        new_survivors = nms(new_boxes, new_scores, 0.6)
        new_boxes = new_boxes[new_survivors]
        new_scores = new_scores[new_survivors]

        new_boxes = torch.stack([box for i, box in enumerate(new_boxes) if new_scores[i] > 0.5], 0)
        new_scores = torch.stack([score for i, score in enumerate(new_scores) if new_scores[i] > 0.5], 0)

    # if all row returned as nan boxes are the ones that have less iou, so we add them
        if len(self.tracks_active):
            distances = mm.distances.iou_matrix(get_xywh(new_boxes), get_xywh(torch.stack([t.box for t in self.tracks_active],0)), 1.0-lambda_new)
            mask = np.all(np.isnan(distances), axis=1)
            new_boxes = new_boxes[mask]
            new_scores = new_scores[mask]
        
            new_box_ind_to_remove = []
            reid = True
            skip_new_box = False
            if reid:
                new_scores = list(new_scores)
                new_boxes = list(new_boxes)
                with torch.no_grad():
                    for box_score_ind, (new_box, new_score) in enumerate(zip(new_boxes, new_scores)):
                        if skip_new_box:
                            skip_new_box = False
                            break
                        for curr_frame in self.tracks_dead_recent:
                            if skip_new_box:
                                break
                            for dead_track_id, dead_track in enumerate(curr_frame):
                                new_box = clip_boxes_to_image(new_box.unsqueeze(0), (frame['img'][0].shape[1], frame['img'][0].shape[2])).squeeze()
                                curr_box = frame['img'][0][:, int(new_box[1]):int(new_box[3]), int(new_box[0]):int(new_box[2])]
                                # similarity model throws error if cropped box is too small
                                im1 = curr_box
                                if curr_box.shape[1] < 50 or curr_box.shape[2] < 50:
                                    im1 = F.interpolate(torch.unsqueeze(curr_box,0), size=50).squeeze()
                                # similarity model expects same size of images for comparison
                                im2 = F.interpolate(torch.unsqueeze(dead_track.tensor, 0), size=(im1.shape[1], im1.shape[2])).squeeze()
                                vis = False
                                if vis:
                                    plt.rcParams["figure.figsize"] = (12,8) # w, h
                                    plt.imshow(torch.cat((im1, im2), 1).permute(1,2,0).numpy())
                                    plt.show()
                                perceptual_loss = self.model(im1, im2)
                                if perceptual_loss.item() < 0.12:
                                #     print(perceptual_loss.item())
                                    self.reid_counter += 1
                                    # update track
                                    dead_track.score = new_score
                                    dead_track.box = new_box
                                    self.tracks_active.append(dead_track)

                                    # not dead track anymore
                                    curr_frame.pop(dead_track_id)

                                    # new box is assigned already, otw it will be added
                                    new_box_ind_to_remove.append(box_score_ind)
                                    new_boxes.pop(box_score_ind)
                                    new_scores.pop(box_score_ind)
                                    
                                    # don't try to assign new box anymore
                                    skip_new_box = True
                                    break
                if len(new_boxes):
                    new_boxes = torch.stack(new_boxes, 0) # list to tensor
                if len(new_scores):
                    new_scores = torch.stack(new_scores, 0)
                                

        self.add(new_boxes, new_scores)
        
        # print([t in self.tracks_active])
        # results
        for t in self.tracks_active:
            if t.id not in self.results.keys():
                self.results[t.id] = {}
            self.results[t.id][self.im_index] = np.concatenate([t.box.cpu().numpy(), np.array([t.score])])

        self.im_index += 1


    def get_results(self):
        return self.results


class Track(object):
    """This class contains all necessary for every individual track."""

    def __init__(self, box, score, track_id):
        self.id = track_id
        self.box = box
        self.score = score
        self.tensor = None

    def get_xywh(self):
        w = self.box[2] - self.box[0]
        h = self.box[3] - self.box[1]
        return torch.tensor([self.box[0], self.box[1], w, h]).to(self.box.device)

    def __str__(self):
        return "id: {}, box: {}, score: {}".format(self.id, self.box, self.score)
